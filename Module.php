<?php

namespace patricy\taggyiing;

use Yii;
use yii\base\UserException;
use yii\db\Query;
use yii\db\Schema;
use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface{

    const ACTION_TABLE_CHECK_IGNORE         = 0;
    const ACTION_TABLE_CHECK_CREATE_TABLES  = 1;
    const ACTION_TABLE_CHECK_EXCEPTION      = 2;
    const ACTION_TABLE_CHECK_ERROR_LOG      = 3;
    
    const TEMPLATE_TAGS_SIMPLE              = 'tags-simple';
    const TEMPLATE_TAGS_BOOTSTRAP_LABELS    = 'tags-bootstrap';
    
    public $controllerNamespace;

    public $tagModels                       = [];
    public $tagTableName                    = 'tag';
    public $tagTableColumns                 = [
                                                'id'            => 'pk',
                                                'name'          => 'string', 
                                                'description'   => 'string',
                                                'slug'          => 'string'
                                            ];
    public $checkTables                     = YII_DEBUG;
    public $actionWhenTableNotMatch         = self::ACTION_TABLE_CHECK_CREATE_TABLES;

    public $tagRender                       = [
                        'grid-concat' =>
                                [
                                    'template'                  => '<{tag-container} class="{tag-class}">{name}</{tag-container}>{tags-separator}',
                                    'tagSeparator'              => ', ',//null for list
                                    'tagContainer'              => 'span',
                                    'tagCssClass'               => 'tag-name',
                                    'tagListContainer'          => 'div',//add li automatically if ul
                                    'tagListContainerCssClass'  => 'tag-list'
                                ],
                        'grid-column' =>
                                [
                                    'template'                  => '<{tag-container} class="{tag-class}">{name}</{tag-container}>{tags-separator}',
                                    'tagSeparator'              => ', ',//null for list
                                    'tagContainer'              => 'span',
                                    'tagCssClass'               => 'tag-name',
                                    'tagListContainer'          => 'div',//add li automatically if ul
                                    'tagListContainerCssClass'  => 'tag-list'
                                ],
                        'listview' =>
                                [
                                    'template'                  => '<{tag-container} class="{tag-class}">{name}</{tag-container}>{tags-separator}',
                                    'tagSeparator'              => ', ',//null for list
                                    'tagContainer'              => 'span',
                                    'tagCssClass'               => 'tag-name',
                                    'tagListContainer'          => 'div',//add li automatically if ul
                                    'tagListContainerCssClass'  => 'tag-list',

                                ]
                        ];

    public function init() {
        if ($this->checkTables) {
            $this->checkTables();
        }


        parent::init();
    }

    public function checkTables() {
        //check Tags table
        $tagTable = Yii::$app->db->schema->getTableSchema($this->tagTableName);
        if (is_null($tagTable) || !empty(array_diff(array_keys($this->tagTableColumns), $tagTable->getColumnNames()))) {
            //tag table not match pattern
            switch ($this->actionWhenTableNotMatch) {
                case self::ACTION_TABLE_CHECK_IGNORE:
                    Yii::error('Tag table not exists or does not match pattern');
                    break;
                case self::ACTION_TABLE_CHECK_CREATE_TABLES:
                    $connection = new Query;

                    if (!is_null($tagTable)){
                        //dropping wrong Tag table
                        $connection->createCommand()->dropTable($this->tagTableName)->execute();
                        Yii::info('Wrong tag table "' . $this->tagTableName . '" dropped');
                    }
                    $connection->createCommand()->createTable($this->tagTableName,$this->tagTableColumns)->execute();
                    Yii::info('Tag table "' . $this->tagTableName . '" created');
                    break;
                case self::ACTION_TABLE_CHECK_EXCEPTION:
                    throw new UserException('Tags table error');
                default:
                    throw new UserException('Tags table error');
            }
        }




    }

    /**
     * Method for module boostrap init. 
     * Adding URL rules
     * @param object $app
     */
    public function bootstrap($app){
        $this->controllerNamespace = __NAMESPACE__ . '\controllers';
        $app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => $this->id, 
                'route' => $this->id . '/tag/index'
            ],
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => $this->id . '/<action:[\w\-]+>/<id:[\d]+>', 
                'route' => $this->id . '/tag/<action>/<id>'
            ],
        ], false);
    }

}
