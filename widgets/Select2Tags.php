<?php
namespace patricy\taggyiing\widgets;

use kartik\select2\Select2;
use Yii;
use patricy\taggyiing\models\Tag;
use yii\bootstrap\Html;
use yii\widgets\InputWidget;


class Select2Tags extends InputWidget
{

    public function run()
    {
        return Html::tag('div', Select2::widget([
             'model' => $this->model,
             'attribute' => 'tags',
             'name' => 'tags',
             'data' => Tag::getTagList(),
             'options' => [
                 'placeholder' => Yii::t('app','Select tags'),
                 'class' => 'form-control',
                 'multiple' => true,
             ],]), ['class'=>'form-group']); 
    }
}