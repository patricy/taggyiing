<?php
namespace patricy\taggyiing\grid;


use yii\bootstrap\Html;

use yii\grid\DataColumn;
use patricy\taggyiing\models\Tag;
use patricy\taggyiing\i18n\Formatter;

class TagsColumn extends DataColumn
{
    public $cellTagName = 'div';
    public $cellOptions = [];
    public $tagOptions = [
        'class' => 'label label-default'
    ];

    protected function renderHeaderCellContent()
    {
        return 'Tags';
    }

    protected function renderFilterCellContent()
    {
        $this->attribute = 'tags';
        $this->filter = Tag::getTagList();
        return parent::renderFilterCellContent();
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        $result = '';
        $formatter = new Formatter;
        
        if (!empty($model->tags)){
            $result = $formatter->asPrettyTags($model->tags);
        }
        return Html::tag($this->cellTagName, $result, $this->cellOptions);
    }
    
}
