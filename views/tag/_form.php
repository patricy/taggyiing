<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use patricy\taggyiing\models\Tag;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
/* @var $this yii\web\View */
/* @var $model frontend\modules\blog\models\Tag */
/* @var $form yii\widgets\ActiveForm */

$tagList = Tag::getTagList();
?>

<div class="tag-form">

    <?php $form = ActiveForm::begin([
        'id' => 'tag-form',
        'enableAjaxValidation' => true,
        'validationUrl' => 'validation-rul',
    ]); ?>

    <?= $form->field($model, 'name')->widget(TypeaheadBasic::classname(), [
        'data' => $tagList,
        'options' => ['placeholder' => 'Enter tag ...'],
        'pluginOptions' => ['highlight'=>true],
    ]);?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
