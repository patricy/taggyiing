<?php

/**
 * @link https://github.com/2amigos/yii2-taggable-behavior
 * @copyright Copyright (c) 2013-2016 2amigOS! Consulting Group LLC
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace patricy\taggyiing\behaviors;

use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\Query;
use patricy\taggyiing\models\Tag;
use yii\validators\SafeValidator;
use yii\web\Controller;
use yii\base\Model;
use yii\web\View;
use Yii;
use yii\db\Schema;

/**
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @author Antonio Ramirez <hola@2amigos.us>
 */
class Taggable extends Behavior {

    /**
     * Tag values
     * @var array
     */
    public $tagIds = [];

    /**
     * @inheritdoc
     */
    public function events() {

        if ($this->owner instanceof Controller) {
            $this->owner->view->on(View::EVENT_BEFORE_RENDER, [$this, 'beforeRenderModelEventHandler']);
            return [];
        }

        if ($this->owner instanceof Model) {
            $this->addSafeValidator();
            return [
                ActiveRecord::EVENT_INIT => 'initModelEventHandler',
                ActiveRecord::EVENT_AFTER_INSERT => 'afterSaveModelEventHandler',
                ActiveRecord::EVENT_AFTER_UPDATE => 'afterSaveModelEventHandler',
            ];
        }
    }

    
    /**
     * Intercepting before view rendering and catching dataProvider. Changing it - 
     * joining tags relation, adding filtering values.
     * Could be disabled from config 
     * 
     * @param type $view
     * @return boolean
     */
    public function beforeRenderModelEventHandler($view) {
        //TODO search in parameters by class name (instanceof DataProvider)
        if (isset($view->params['dataProvider'])) {
            $query = $view->params['dataProvider']->query;
            $query->joinWith('tags');
            //tag id search
            $query->andFilterWhere([
                'tag.id' => $view->params['searchModel']->tagIds,
            ]);
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true) {
        if ($name === 'tags') {
            return true;
        }
        return parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true) {
        if ($name === 'tags') {
            return true;
        }
        return parent::canSetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value) {
        $this->tagIds = $value;
    }

    /**
     * Relation Tags
     * @return type
     */
    public function getTags() {
        return $this->owner->hasMany(Tag::className(), ['id' => Tag::tableName() . '_id'])
                        ->viaTable(Tag::tableName() . '_' . $this->owner->tableName(), [$this->owner->tableName() . '_id' => 'id']);
    }

    /**
     * 
     * Adding "safe" validator to virtual attribute tags.
     * Adding this validator only if not exists in model
     * @return void
     */
    private function addSafeValidator() {
        if ($this->owner instanceof Model) {
            foreach ($this->owner->validators as $key => $validator) {
                if (is_a($validator, SafeValidator::className())) {
                    if (!in_array('tags', $validator->attributes)) {
                        $this->owner->validators[$key]->attributes[] = 'tags';
                    }
                }
            }
        }
    }

    public function initModelEventHandler() {
        //check relations tables 
        $taggableModel = $this->owner;
        $relationTableName = Tag::tableName() . '_' . $taggableModel::tableName();
        $tagRelationTable = Yii::$app->db->schema->getTableSchema($relationTableName);
        $tagRelationTableColumns = [Tag::tableName() . '_id', $taggableModel::tableName() . '_id'];
        if (is_null($tagRelationTable) || !empty(array_diff($tagRelationTableColumns, $tagRelationTable->getColumnNames()))) {
            //tag relations table not match pattern
            $tagManager = Yii::$app->getModule('tagManager');
            switch ($tagManager->actionWhenTableNotMatch) {
                case $tagManager::ACTION_TABLE_CHECK_IGNORE:
                    //DO NOTHING
                    break;

                case $tagManager::ACTION_TABLE_CHECK_CREATE_TABLES:
                    $connection = new Query;
                    if (!is_null($tagRelationTable)) {
                        //dropping wrong Tag relation table
                        $connection->createCommand()->dropTable($relationTableName)->execute();
                        Yii::info('Wrong tag relation table "' . $relationTableName . '" dropped');
                    }
                    $connection->createCommand()->createTable($relationTableName, [
                        Tag::tableName() . '_id' => Schema::TYPE_INTEGER,
                        $taggableModel::tableName() . '_id' => Schema::TYPE_INTEGER
                    ])->execute();
                    Yii::info('Tag relation table "' . $relationTableName . '" created');
                    $connection->createCommand()->addForeignKey(
                                    'FK_tag_' . $relationTableName . '_tag', $relationTableName, Tag::tableName() . '_id', Tag::tableName(), 'id', 'CASCADE', 'CASCADE'
                            )
                            ->execute();
                    $connection->createCommand()->addForeignKey(
                                    'FK_tag_' . $relationTableName . '_relation_tagged', $relationTableName, $taggableModel::tableName() . '_id', $taggableModel::tableName(), 'id', 'CASCADE', 'CASCADE'
                            )
                            ->execute();
                    Yii::info('FK for relation table "' . $relationTableName . '" added');
                    break;

                case $tagManager::ACTION_TABLE_CHECK_ERROR_LOG:
                    Yii::error('Tag relation table not exists or does not match pattern');
                    break;

                case $tagManager::ACTION_TABLE_CHECK_EXCEPTION:
                    throw new UserException('Tags relation table error');


                default:
                    throw new UserException('Tags relation table error');
            }
        }
    }

    /**
     * @param Event $event
     */
    public function afterSaveModelEventHandler($event) {

        if ($this->tagIds === null || empty($this->tagIds)) {
            $this->tagIds = [];
        }
        $relation = $this->owner->getRelation('tags');

        $junctionTableName = $relation->via->from[0];

        $tagModels = Tag::findAll($this->tagIds);

        if (!$this->owner->getIsNewRecord()) {
             $this->owner->getDb()->createCommand()
                     ->delete($junctionTableName, [$this->owner->tableName() . '_id' => $this->owner->getPrimaryKey()])
                     ->execute();
        }


        foreach ($this->tagIds as $tagId) {
            $columns = [
                $this->owner->tableName() . '_id' => $this->owner->getPrimaryKey(),
                Tag::tableName() . '_id'            => $tagId
            ];
 
            $this->owner->getDb()->createCommand()
                    ->insert($junctionTableName, $columns)
                    ->execute();
        }

        $this->owner->populateRelation('tags', $tagModels);
    }

}
