<?php

error_reporting(-1);

define('YII_DEBUG', true);
define('YII_ENV', 'test');
$_SERVER['SCRIPT_NAME'] = '/' . __DIR__;
$_SERVER['SCRIPT_FILENAME'] = __FILE__;

$parentProjectDir = realpath(__DIR__ . '/../../../..');

require_once $parentProjectDir . '/vendor/autoload.php';
require_once $parentProjectDir . '/vendor/yiisoft/yii2/Yii.php';
require_once __DIR__ . '/TestCase.php';



//require_once $parentProjectDir . '/console/config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require_once $parentProjectDir . '/common/config/main.php',
    require_once $parentProjectDir . '/common/config/main-local.php',
    require_once $parentProjectDir . '/console/config/main.php'
  //  require_once $parentProjectDir . '/console/config/main-local.php'
);
require_once $parentProjectDir . '/common/config/bootstrap.php';

new yii\console\Application($config);

