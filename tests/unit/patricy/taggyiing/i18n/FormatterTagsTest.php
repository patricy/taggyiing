<?php
namespace tests\unit\patricy\taggyiing\i18n;

use yiiunit\TestCase;
use patricy\taggyiing\i18n\Formatter;
use patricy\taggyiing\models\Tag;

class FormatterNumberTest extends TestCase
{

    protected $formatter;

    protected $tags = [];

    protected function setUp()
    {
        parent::setUp();

        $this->formatter = new Formatter();

        $tag1 = new Tag;
        $tag1->name = 'name1';

        $tag2 = new Tag;
        $tag2->name = 'name2';

        $tag3 = new Tag;
        $tag3->name = 'name3';

        $this->tags = [$tag1, $tag2, $tag3];
    }
    protected function tearDown()
    {
       // parent::tearDown();
        $this->formatter = null;
        $this->tags  = [];
    }


    public function testAsTags()
    {
        $this->assertSame('name1, name2, name3', $this->formatter->asTags($this->tags));

        $this->assertSame('', $this->formatter->asTags([]));

        $this->assertSame('', $this->formatter->asTags(null));

        $this->assertSame('', $this->formatter->asTags('some string'));

        unset($this->tags[2]->name);

        $this->assertSame('name1, name2', $this->formatter->asTags($this->tags));

    }
    
    
    public function testAsPrettyTags()
    {
        $this->assertSame('<span class="label label-default">name1</span> <span class="label label-default">name2</span> <span class="label label-default">name3</span>', $this->formatter->asPrettyTags($this->tags));

        $this->assertSame('', $this->formatter->asTags([]));

        $this->assertSame('', $this->formatter->asTags(null));

        $this->assertSame('', $this->formatter->asTags('some string'));

        unset($this->tags[2]->name);

        $this->assertSame('<span class="label label-default">name1</span> <span class="label label-default">name2</span>', $this->formatter->asPrettyTags($this->tags));

    }

}