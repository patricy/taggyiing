<?php

namespace patricy\taggyiing\models;

use yii\behaviors\SluggableBehavior;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property BlogPostTag[] $blogPostTags
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    public static function getTagList(){
        $modelList = Tag::find()->all();
        $result = [];
        foreach($modelList as $model){
            $result[$model->id] = $model->name;
        }

        return $result;
    }




    public function behaviors(){
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name'
            ],
        ];
    }

    public static function renderTagsDropdown($model){
            $result = '<div class="form-group">';
            $result.= Html::label(Yii::t('app','Tags'), 'tags');
            $result.= Select2::widget([
                'model' => $model,
                'attribute' => 'tags',
                'name' => 'tags',
                'data' => self::getTagList(),
                'pluginOptions' => [
                    'tags' => self::getTagList(),
                ],
                'options' => [
                    'placeholder' => Yii::t('app','Select tags'),
                    'class' => 'form-control',
                    'multiple' => true,
                ],]);

            $result.= '</div>';
            return $result;
    }

}
