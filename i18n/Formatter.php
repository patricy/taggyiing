<?php
namespace patricy\taggyiing\i18n;

use yii\i18n\Formatter as BaseFormatter;

class Formatter extends BaseFormatter {
    public function asTags($tagModels, $delimiter = ', ', $itemWrapperStart = '', $itemWrapperEnd = ''){
        $result = [];
        if (!empty($tagModels) && is_array($tagModels)){
            foreach($tagModels as $tagModel){
                if ($tagModel->name){
                    $result[] = $itemWrapperStart . $tagModel->name . $itemWrapperEnd;
                }
            }
        }
        return implode($delimiter, $result);
    }

    public function asPrettyTags($tagModels){
        return $this->asTags($tagModels, ' ', '<span class="label label-default">', '</span>');
    }
}
